<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class loginController extends Controller
{
    function index (){
    
        return view('login');

    }

    function postLogin (Request $request){
    
        // dd($request->all());
        if (Auth::attempt($request->only('email','password'))){
            return ('login berhasil');
        }
        return redirect('/login');
    }


}
